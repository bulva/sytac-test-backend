// process.env.NODE_ENV = 'test';


const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
const supertest = require('supertest');
const express = require('express');
const server = require('./../app');
const api = supertest(server);


describe('sytac SERVER', () => {

    it('front-end to return a 200 response', (done) => {
      api.get('/')
      .set('Accept', 'text/html; charset=UTF-8')
      .expect(200,done);
    });

    it('back-end to return a 200 response', (done) => {
      api.get('/api')
      .set('Accept', 'application/json')
      .expect(200,done);
    });

    it('back-end to return a 404 response', (done) => {
      api.get('/xyz')
      .set('Accept', 'application/json')
      .expect(404,done);
    });

});

describe('sytac API', () => {

    it('api is alive - 200 response', (done) => {
      api.get('/api')
      .set('Accept', 'application/json')
      .expect(200,done);
    });

    it('should be an object with keys and values', (done) => {
      api.get('/api')
      .set('Accept', 'application/json')
      .expect(200)
      .end((err, res) => {
        expect(res.body.length).to.not.equal(0);

        expect(res.body[0]).to.have.property("id");
        expect(res.body[0]).to.have.property("type");
        expect(res.body[0]).to.have.property("brand");
        expect(res.body[0]).to.have.property("colors");
        expect(res.body[0]).to.have.property("img");

        expect(res.body[0].id).to.not.equal(null);
        expect(res.body[0].type).to.not.equal(null);
        expect(res.body[0].brand).to.not.equal(null);
        expect(res.body[0].colors).to.not.equal(null);
        expect(res.body[0].img).to.not.equal(null);

        done();
      });
    });
});