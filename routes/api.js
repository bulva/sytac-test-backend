var express = require('express');
var trafficMeisterService = require('./../services/trafficMeister');

var router = express.Router();

// placed in scope closure
(() => {

  // define route, GET on root
  router.get('', function (req, res) {

    try{

      trafficMeisterService.fetchData((error, data) => {

        // handling errors
        if ( error ) {
          res.status(503);
          res.send(error);
        }

        res.json(data);

      });

    }
    catch(error) {

      res.status(503);
      res.send(error.message || 'trafficMeisterService.fetchData() Error');

    }
    
  });


  if (typeof module !== 'undefined') {
    module.exports = router;
  }
})();