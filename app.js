const debug = require('debug')('sytac:app');

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const helmet = require('helmet');

const app = express();


(() => {

  // basic security
  // dissable Strict-Transport-Security for subdomains
  // it might becausing problems with https redirecting

  helmet.hsts({
    maxAge: 5184000, 
    includeSubDomains: false
  });

  // Set headers
  app.use((req, res, next) => {

    // this is only for developement 
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4500');
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
  });

  app.use(helmet());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use('/',express.static(path.join(__dirname, 'public')));

  if(process.env.NODE_ENV === 'test') {
    app.use('/tests', express.static(path.join(__dirname, 'tests')));
  }

  // routing
  const apiRoutes = require('./routes/api.js');

  // api root path definition
  app.use('/api', apiRoutes);

  // catch all route
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use((err, req, res, next) => {
    // console.log('error handler');
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error response
    // as this is only api we don't need to render html
    res.status(err.status || 500);
    res.end();
  });


  if (typeof module !== 'undefined') {
    module.exports = app;
  }
})();
