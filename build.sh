#!/bin/bash

ROOT=$PWD

if [ -d "$ROOT/public" ]; then
  rm -rf $ROOT/public;
fi

if [ ! -d "node_modules/" ]; then
    npm install;
fi

if [ -d "$ROOT/../sytac-test-frontend" ]; then

  cd "$ROOT/../sytac-test-frontend";

  if [ -d "dist/" ]; then
    rm -rf dist/;
  fi

  if [ ! -d "node_modules/" ]; then
    npm install;
  fi

  npm run build

  mv dist $ROOT/public
  cd $ROOT;

  echo "Build done, run via -- npm start";
else 
  echo "Frontend directory not found! Exiting." 
fi