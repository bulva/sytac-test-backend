FROM node:carbon AS dist

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production
RUN npm install forever

COPY . .

EXPOSE 8080

ENTRYPOINT [ "forever", "bin/www" ]



FROM node:carbon AS dev

RUN apt-get update
RUN apt-get install -y \
    wget \
    imagemagick 

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
RUN npm install -g nodemon

EXPOSE 8080

ENTRYPOINT [ "nodemon" ]